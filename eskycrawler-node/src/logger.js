'use strict';

const logger = require('winston');

logger.add(new logger.transports.File({
  name: 'debug-file',
  filename: 'log.log',
  level: 'debug',
  handleExceptions: true,
  humanReadableUnhandledException: true,
  exitOnError: true,
  json: false,
  maxsize: 104857600,
  maxFiles: 5,
}));

logger.add(new logger.transports.Console({
  name: 'error-console',
  level: 'info',
  handleExceptions: true,
  humanReadableUnhandledException: true,
  exitOnError: true,
}));

module.exports = logger;
