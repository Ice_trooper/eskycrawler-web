'use strict';

const {
  dbHost,
  dbPort,
  dbName,
  dbUser,
  dbPass,
  dbCharset,
} = require('./config.js');

module.exports = {
  test: {
    client: 'mysql2',
    connection: {

    },
    // migrations: {
    //   directory: __dirname + '/db/migrations'
    // },
    // seeds: {
    //   directory: __dirname + '/db/seeds/test'
    // }
  },
  development: {
    debug: true,
    client: 'mysql2',
    connection: {
      host: dbHost,
      port: dbPort,
      database: dbName,
      charset: dbCharset,
      user: dbUser,
      password: dbPass,
      timezone: 'UTC',
      // dateStrings: true,
    },
    // pool: {
    //   min: 2,
    //   max: 10,
    // },
    // migrations: {
    //   directory: __dirname + '/db/migrations'
    // },
    // seeds: {
    //   directory: __dirname + '/db/seeds/development'
    // }
  },
  production: {
    client: 'mysql2',
    // migrations: {
    //   directory: __dirname + '/db/migrations'
    // },
    // seeds: {
    //   directory: __dirname + '/db/seeds/production'
    // }
  },
};

// module.exports = {
//   development: {
//     client: 'sqlite3',
//     useNullAsDefault: true,
//     connection: {
//       filename: './example.db'
//     },
//     pool: {
//       afterCreate: (conn, cb) => {
//         conn.run('PRAGMA foreign_keys = ON', cb);
//       }
//     }
//   },

//   production: {
//     client: 'postgresql',
//     connection: {
//       database: 'example'
//     },
//     pool: {
//       min: 2,
//       max: 10
//     }
//   }
// };


// module.exports = {
//   development: {
//       client: ‘mysql’,
//       connection: {
//       host: ‘127.0.0.1’,
//       user: ‘root’, // replace with your mysql username
//       password: ‘123456’, // replace with your mysql password
//       database: ‘objection_crud’
//     },
//     debug: true
//   }
// };
