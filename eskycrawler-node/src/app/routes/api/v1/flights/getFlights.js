'use strict';

const { Router } = require('express');

module.exports = Router({ mergeParams: true })
  .get('/v1/flights', async (req, res, next) => {
    try {
      // const flights = await req.db.Country.query().select('codeName', 'name')

      res.sendStatus(200);
    } catch (error) {
      next(error);
    }
  });
