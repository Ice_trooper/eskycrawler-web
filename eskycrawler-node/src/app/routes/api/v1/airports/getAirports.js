'use strict';

const { Router } = require('express');

module.exports = Router({ mergeParams: true })
  .get('/v1/airports', async (req, res, next) => {
    try {
      const airportsQuery = req.db.Airport.query();

      airportsQuery
        .select('airports.id', 'airports.name', 'airports.codeName')
        .select('city.name as cityName', 'city.codeName as cityCodeName')
        .select('city:country.name as countryName', 'city:country.codeName as countryCodeName')
        .joinRelated('city.country');

      const airports = await airportsQuery;

      res.send(airports);
    } catch (error) {
      next(error);
    }
  });
