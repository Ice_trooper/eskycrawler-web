'use strict';

const { Router } = require('express');

module.exports = Router({ mergeParams: true })
  .get('/v1/airlines/:uuid', async (req, res, next) => {
    try {
      const airline = await req.db.Airline.query().where('id', req.params.uuid).select('codeName', 'name');

      res.send(airline);
    } catch (error) {
      next(error);
    }
  });
