'use strict';

const { Router } = require('express');

module.exports = Router({ mergeParams: true })
  .get('/v1/airlines', async (req, res, next) => {
    try {
      const airlines = await req.db.Airline.query();// .select('codeName', 'name');

      res.send(airlines);
    } catch (error) {
      next(error);
    }
  });
