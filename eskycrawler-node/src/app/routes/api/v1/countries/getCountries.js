'use strict';

const { Router } = require('express');

module.exports = Router({ mergeParams: true })
  .get('/v1/countries', async (req, res, next) => {
    try {
      const countries = await req.db.Country.query().select('codeName', 'name');

      res.send(countries);
    } catch (error) {
      next(error);
    }
  });
