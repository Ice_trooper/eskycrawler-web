'use strict';

const { Router } = require('express');

module.exports = Router({ mergeParams: true })
  .get('/v1/cities', async (req, res, next) => {
    try {
      const cities = await req.db.City.query().select('codeName', 'name');

      res.send(cities);
    } catch (error) {
      next(error);
    }
  });
