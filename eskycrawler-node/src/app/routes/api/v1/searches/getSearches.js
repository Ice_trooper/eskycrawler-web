'use strict';

const { Router } = require('express');

module.exports = Router({ mergeParams: true })
  .get('/v1/searches', async (req, res, next) => {
    try {
      const searches = await req.db.Search.query();

      res.send(searches);
    } catch (error) {
      next(error);
    }
  });
