'use strict';

const { Router } = require('express');
const { raw } = require('objection');

module.exports = Router({ mergeParams: true })
  .get('/v1/calendar', async (req, res, next) => {
    try {
      const maxedSearch = Number.isNaN(Number(req.query.search))
        ? req.db.Offer.query().max('search_id').first()
        : req.query.search;

      const calendarQuery = req.db.Offer
        .query()
        .where('search_id', maxedSearch);

      calendarQuery
        .joinRelated('[outboundFlight, inboundFlight]')
        .count('* as offersCount')
        .min('price as lowestPrice')
        .groupByRaw('date')
        .orderBy('date', 'asc');

      if (req.query.returns === 'true') {
        calendarQuery.select(raw('DATE(FROM_UNIXTIME(inboundFlight.from_date / 1000)) AS date'));
      } else {
        calendarQuery.select(raw('DATE(FROM_UNIXTIME(outboundFlight.from_date / 1000)) AS date'));
      }

      const dateStart = new Date(req.query.start);
      if (+dateStart) {
        calendarQuery.modify('onlyOffersFromDate', dateStart);
      }

      const dateEnd = new Date(req.query.end);
      if (+dateEnd) {
        calendarQuery.modify('onlyOffersToDate', dateEnd);
      }

      const dateDeparture = new Date(req.query.departureDate);
      if (+dateDeparture) {
        calendarQuery.modify('onlyDeparturesInDay', dateDeparture);
      }

      const dateReturn = new Date(req.query.returnDate);
      if (+dateReturn) {
        // dateReturn.setHours(23, 59, 59, 999);
        calendarQuery.modify('onlyReturnsInDay', dateReturn);
      }

      const calendar = await calendarQuery;

      res.send(calendar);
    } catch (error) {
      next(error);
    }
  });
