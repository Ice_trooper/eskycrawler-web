'use strict';

const { Router } = require('express');
// const { Offer, Airline, Airport} = require('../../../../db')

module.exports = Router({ mergeParams: true })
  .get('/v1/offers', async (req, res, next) => {
    try {
      const maxedSearch = Number.isNaN(Number(req.query.search))
        ? req.db.Offer.query().max('search_id').first()
        : req.query.search;

      const offersQuery = req.db.Offer
        .query()
        .where('search_id', maxedSearch);

      // offersQuery = offersQuery
      // .joinRelation('[outboundFlight.[airportFrom, airline], inboundFlight]');

      // if (req.query.groupBy === 'day') {
      // offersQuery.count('offers.id as offersCount')
      // .select('price').groupBy('price').orderBy('price', 'asc');
      // }

      offersQuery.withGraphJoined('[outboundFlight, inboundFlight]');

      const dateStart = new Date(req.query.start);
      if (+dateStart) {
        offersQuery.modify('onlyOffersFromDate', dateStart);
      }

      const dateEnd = new Date(req.query.end);
      if (+dateEnd) {
        offersQuery.modify('onlyOffersToDate', dateEnd);
      }

      const dateDeparture = new Date(req.query.departureDate);
      if (+dateDeparture) {
        offersQuery.modify('onlyDeparturesInDay', dateDeparture);
      }

      const dateReturn = new Date(req.query.returnDate);
      if (+dateReturn) {
        // dateReturn.setHours(23, 59, 59, 999);
        offersQuery.modify('onlyReturnsInDay', dateReturn);
      }

      const offers = await offersQuery;

      res.send(offers);
    } catch (error) {
      next(error);
    }
  });
