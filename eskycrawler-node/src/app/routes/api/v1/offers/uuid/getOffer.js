'use strict';

const { Router } = require('express');

module.exports = Router({ mergeParams: true })
  .get('/v1/offers/:uuid', async (req, res, next) => {
    try {
      const offer = await req.db.Offer.query().where('search_id', req.params.uuid);

      if (!offer) {
        // throw
      }

      res.send(offer);
    } catch (error) {
      next(error);
    }
  });

// const Router = require('express').Router

// module.exports = Router({ mergeParams: true })
//   .post('/v1/users', async (req, res, next) => {
//     try {
//       const user = new req.db.User({
//         username: req.body.username,
//         password: req.body.password,
//       })
//       await user.save()
//       const location = `${req.base}${req.originalUrl}/${user.id}`
//       res.setHeader('Location', location)
//       res.status(201).send(user)
//     } catch (error) {
//       next(error)
//     }
//   })


// const Router = require('express').Router

// module.exports = Router({ mergeParams: true })
//   .put('/v1/users/:id/password', async (req, res, next) => {
//     try {
//       const user = await req.db.User.findById(req.params.id)
//       user.password = req.body.password
//       await user.save()
//       user.sayHello()
//       res.sendStatus(204)
//     } catch (error) {
//       next(error)
//     }
//   })
