/* eslint-disable global-require */

'use strict';

const glob = require('glob');
const path = require('path');
const Knex = require('knex');
const { Model } = require('objection');
const knexConfig = require('../../knexfile');

// var environment = process.env.NODE_ENV || 'development'
// var config = require('../knexfile.js')[environment]

// module.exports = require('knex')(config)

// Initialize knex.
const knex = Knex(knexConfig.development);

// Bind all Models to a knex instance. If you only have one database in
// your server this is all you have to do. For multi database systems, see
// the Model.bindKnex method.
Model.knex(knex);

module.exports = knex;

const db = glob.sync('./models/**/*.js', { cwd: __dirname });
const models = {};
db.forEach((filename) => {
  const model = require(filename);
  models[path.basename(filename).split('.')[0]] = model;
});

module.exports = (/* { logger } */) => models;
