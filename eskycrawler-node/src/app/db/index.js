'use strict';

const Airline = require('./models/Airline');
const Airport = require('./models/Airport');
const City = require('./models/City');
const Country = require('./models/Country');
const Flight = require('./models/Flight');
const Offer = require('./models/Offer');
const Search = require('./models/Search');

module.exports = {
  Airline,
  Airport,
  City,
  Country,
  Flight,
  Offer,
  Search,
};
