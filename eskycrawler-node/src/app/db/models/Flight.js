/* eslint-disable global-require */

'use strict';

const { Model, snakeCaseMappers } = require('objection');

class FlightModel extends Model {
  static tableName = 'flights';

  static modifiers = {
    defaultSelects(query) {
      const { ref } = FlightModel;
      query.select(
        ref('id'),
        ref('flight_number'),
        ref('duration'),
        ref('from_date'),
        ref('to_date'),
        ref('airline_id'),
        ref('from_airport_id'),
        ref('to_airport_id'),
      );
    },
    onlyDepartureInDate(query, date) {
      const dBegin = new Date(date);
      dBegin.setUTCHours(0, 0, 0, 0);
      const dEnd = new Date(date);
      dEnd.setUTCHours(23, 59, 59, 999);
      if (+date) {
        const { ref } = FlightModel;
        query.whereBetween(ref('from_date'),
          [dBegin.getTime(), dEnd.getTime()]);
      }
    },
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    // Importing models here is a one way to avoid require loops.
    const Offer = require('./Offer');
    const Airline = require('./Airline');
    const Airport = require('./Airport');

    return {
      airportFrom: {
        relation: Model.BelongsToOneRelation,

        modelClass: Airport,
        join: {
          from: 'flights.from_airport_id',
          to: 'airports.id',
        },
      },
      airportTo: {
        relation: Model.BelongsToOneRelation,

        modelClass: Airport,
        join: {
          from: 'flights.to_airport_id',
          to: 'airports.id',
        },
      },
      airline: {
        relation: Model.BelongsToOneRelation,

        modelClass: Airline,
        join: {
          from: 'flights.airline_id',
          to: 'airlines.id',
        },
      },
      offersOutboundFlight: {
        relation: Model.HasManyRelation,

        modelClass: Offer,
        join: {
          from: 'flights.id',
          to: 'offers.outbound_flight_id',
        },
      },
      offersInboundFlight: {
        relation: Model.HasManyRelation,

        modelClass: Offer,
        join: {
          from: 'flights.id',
          to: 'offers.inbound_flight_id',
        },
      },
    };
  }

  static get columnNameMappers() {
    return snakeCaseMappers();
  }
}

module.exports = FlightModel;
