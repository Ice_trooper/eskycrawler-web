/* eslint-disable global-require */

'use strict';

const { Model, snakeCaseMappers } = require('objection');

class CountryModel extends Model {
  static tableName = 'countries';

  // This object defines the relations to other models.
  static get relationMappings() {
    // Importing models here is a one way to avoid require loops.
    const Cities = require('./City');

    return {
      cities: {
        relation: Model.HasManyRelation,

        modelClass: Cities,
        join: {
          from: 'countries.id',
          to: 'cities.country_id',
        },
      },
    };
  }

  static get columnNameMappers() {
    return snakeCaseMappers();
  }
}

module.exports = CountryModel;
