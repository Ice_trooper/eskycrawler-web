/* eslint-disable global-require */

'use strict';

const { Model, snakeCaseMappers } = require('objection');

class SearchModel extends Model {
  static tableName = 'searches';

  // This object defines the relations to other models.
  static get relationMappings() {
    // Importing models here is a one way to avoid require loops.
    const Offer = require('./Offer');

    return {
      offers: {
        relation: Model.HasManyRelation,

        modelClass: Offer,
        join: {
          from: 'searches.id',
          to: 'offers.search_id',
        },
      },
    };
  }

  static get columnNameMappers() {
    return snakeCaseMappers();
  }
}

module.exports = SearchModel;
