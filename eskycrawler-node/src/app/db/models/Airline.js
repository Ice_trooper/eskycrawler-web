/* eslint-disable global-require */

'use strict';

const { Model, snakeCaseMappers } = require('objection');

class AirlineModel extends Model {
  static tableName = 'airlines';

  // This object defines the relations to other models.
  static get relationMappings() {
    // Importing models here is a one way to avoid require loops.
    const Flight = require('./Flight');

    return {
      flights: {
        relation: Model.HasManyRelation,

        modelClass: Flight,
        join: {
          from: 'airlines.id',
          to: 'flights.airline_id',
        },
      },
    };
  }

  static get columnNameMappers() {
    return snakeCaseMappers();
  }
}

module.exports = AirlineModel;
