/* eslint-disable global-require */

'use strict';

const { Model, snakeCaseMappers } = require('objection');

class OfferModel extends Model {
  static tableName = 'offers';

  static modifiers = {
    defaultSelects(query) {
      const { ref } = OfferModel;
      query.select(
        ref('id'),
        ref('url'),
        ref('price'),
        ref('search_id'),
        ref('outbound_flight_id'),
        ref('inbound_flight_id'),
      );
    },
    // onlyOutboundsWithDeparturesInDate
    onlyDeparturesInDay(query, date) {
      const dBegin = new Date(date);
      dBegin.setUTCHours(0, 0, 0, 0);
      const dEnd = new Date(date);
      dEnd.setUTCHours(23, 59, 59, 999);
      query.whereBetween('outboundFlight.from_date',
        [dBegin.getTime(), dEnd.getTime()]);
    },
    // support for arrays of dates?
    onlyReturnsInDay(query, date) {
      const dBegin = new Date(date);
      dBegin.setUTCHours(0, 0, 0, 0);
      const dEnd = new Date(date);
      dEnd.setUTCHours(23, 59, 59, 999);
      query.whereBetween('inboundFlight.from_date',
        [dBegin.getTime(), dEnd.getTime()]);
    },
    onlyOffersFromDate(query, date) {
      query.where('outboundFlight.from_date', '>=', date.getTime());
    },
    onlyOffersToDate(query, date) {
      query.where('outboundFlight.from_date', '<=', date.getTime());
    },
  };

  // This object defines the relations to other models.
  static get relationMappings() {
    // Importing models here is a one way to avoid require loops.
    const Search = require('./Search');
    const Flight = require('./Flight');

    return {
      search: {
        relation: Model.BelongsToOneRelation,

        modelClass: Search,
        join: {
          from: 'offers.search_id',
          to: 'searches.id',
        },
      },
      outboundFlight: {
        relation: Model.BelongsToOneRelation,

        modelClass: Flight,
        join: {
          from: 'offers.outbound_flight_id',
          to: 'flights.id',
        },
      },
      inboundFlight: {
        relation: Model.BelongsToOneRelation,

        modelClass: Flight,
        join: {
          from: 'offers.inbound_flight_id',
          to: 'flights.id',
        },
      },
    };
  }

  static get columnNameMappers() {
    // If your columns are UPPER_SNAKE_CASE you can
    // use snakeCaseMappers({ upperCase: true })
    return snakeCaseMappers();
  }
}

module.exports = OfferModel;
