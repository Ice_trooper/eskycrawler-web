/* eslint-disable global-require */

'use strict';

const { Model, snakeCaseMappers } = require('objection');

class AirportModel extends Model {
  static tableName = 'airports';

  // This object defines the relations to other models.
  static get relationMappings() {
    // Importing models here is a one way to avoid require loops.
    const City = require('./City');
    const Flight = require('./Flight');

    return {
      city: {
        relation: Model.BelongsToOneRelation,

        modelClass: City,
        join: {
          from: 'airports.city_id',
          to: 'cities.id',
        },
      },
      flightsFromAirport: {
        relation: Model.HasManyRelation,

        modelClass: Flight,
        join: {
          from: 'airports.id',
          to: 'flights.from_airport_id',
        },
      },
      flightsToAirport: {
        relation: Model.HasManyRelation,

        modelClass: Flight,
        join: {
          from: 'airports.id',
          to: 'flights.to_airport_id',
        },
      },
    };
  }

  static get columnNameMappers() {
    return snakeCaseMappers();
  }
}

module.exports = AirportModel;
