/* eslint-disable global-require */

'use strict';

const { Model, snakeCaseMappers } = require('objection');

class CityModel extends Model {
  static tableName = 'cities';

  // This object defines the relations to other models.
  static get relationMappings() {
    // Importing models here is a one way to avoid require loops.
    const Country = require('./Country');
    const Airport = require('./Airport');

    return {
      country: {
        relation: Model.BelongsToOneRelation,

        modelClass: Country,
        join: {
          from: 'cities.country_id',
          to: 'countries.id',
        },
      },
      airports: {
        relation: Model.HasManyRelation,

        modelClass: Airport,
        join: {
          from: 'cities.id',
          to: 'airports.city_id',
        },
      },
    };
  }

  static get columnNameMappers() {
    return snakeCaseMappers();
  }
}

module.exports = CityModel;
