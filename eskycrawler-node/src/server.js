'use strict';

const server = require('http').createServer();
const logger = require('./logger.js');
const database = require('./app/db/create-database.js')({ logger });
const app = require('./app/create-express-app.js')({ logger, database });
const { port } = require('./config.js');

server
  .on('request', app)
  .on('listening', () => {
    const addr = server.address();
    const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
    logger.info(`Listening on ${bind}`);
    // console.log(`Listening on ${bind}`)
  })
  .on('error', (error) => {
    if (error.syscall !== 'listen') throw error;
    const addr = server.address() || { port };
    const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
    switch (error.code) {
      case 'EACCES':
        logger.error(`${bind} requires elevated privileges`);
        process.exit(1);
        break;
      case 'EADDRINUSE':
        logger.error(`${bind} is already in use`);
        process.exit(1);
        break;
      default:
        throw error;
    }
  })
  .listen(port);


// https://medium.com/@carlos.illobre/nodejs-express-how-to-organize-your-routes-in-very-big-applications-and-why-controllers-are-evil-e202eea497f4
