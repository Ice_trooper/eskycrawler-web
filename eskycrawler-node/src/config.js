'use strict';

module.exports = {
  environment: process.env.NODE_ENV || 'development',
  port: process.env.NODE_PORT || 8080,
  dbHost: process.env.DB_HOST,
  dbPort: process.env.DB_PORT,
  dbName: process.env.DB_NAME,
  dbCharset: process.env.DB_CHARSET,
  dbUser: process.env.DB_USER,
  dbPass: process.env.DB_ACCESS_KEY,
};
