import { createMuiTheme } from "@material-ui/core";

export default () => {
  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#7b1fa2"
      }
    },
    zIndex: {
      mobileStepper: 1000,
      speedDial: 1050,
      drawer: 1100,
      appBar: 1200,
      modal: 1300,
      snackbar: 1400,
      tooltip: 1500
    }    
  })
  console.log(theme)
  return theme;
}