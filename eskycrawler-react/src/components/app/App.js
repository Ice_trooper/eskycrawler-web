import React from 'react';
// import logo from '../../assets/images/logo.svg';
import './App.css';
import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from '@material-ui/styles';
// import { Root, Header, Nav, Content, Footer, presets } from "mui-layout";
import { Root } from 'pages'

import createTheme from './MuiTheme'
import AirportProvider from 'context/airports/AirportContext';
import AirlineProvider from 'context/airlines/AirlineContext';

export default function App() {
  return (
    <AirportProvider>
      <AirlineProvider>
        <ThemeProvider theme={createTheme()}>
          <CssBaseline />
          <Root />
        </ThemeProvider>
      </AirlineProvider>
    </AirportProvider>
  )
}

// function App() {
//   return (
//     <ThemeProvider theme={baseTheme}>
//       <div className="App">
//         <Root config={config} style={{ minHeight: "100vh" }}>
//           <CssBaseline />
//           <Header
//             renderMenuIcon={open => (open ? <ChevronLeftIcon /> : <MenuIcon />)}
//           >
//             <MainAppbar />
//               {/* header goes here */}
//           </Header>
//           <Nav
//             renderIcon={collapsed =>
//               collapsed ? <ChevronRightIcon /> : <ChevronLeftIcon />
//             }
//             header={
//               // you can provide fixed header inside nav
//               // change null to some react element
//               ctx => null
//             }
//           >
//             {/* nav goes here */}
//           </Nav>
//           <Content>
//             <Typography variant="h5">Content</Typography>
//             <MainAppLogo img={logo}/>
//             <FlightsSearchForm />
//             <Button
//               variant="contained"
//               color="primary"
//               // className={classes.button}
//               endIcon={<SearchIcon />}
//             >
//               Filter
//             </Button>
//             <FlightsCalendar />
//           </Content>
//           <Footer><Typography variant="subtitle1">Footer</Typography>{/* footer goes here */}</Footer>
//         </Root>

//         {/* <header className="App-header">
//           <MainAppbar/>
//           <img src={logo} className="App-logo" alt="logo" />
//           <p>
//             Edit <code>src/App.js</code> and save to reload.
//           </p>
//           <a
//             className="App-link"
//             href="https://reactjs.org"
//             target="_blank"
//             rel="noopener noreferrer"
//           >
//             Learn React
//           </a>
//         </header>
//         <body>
//           <Container>
//             <MainAppLogo img={logo}/>
//           </Container>
//         </body> */}
//       </div>
//     </ThemeProvider>
//   );
// }
