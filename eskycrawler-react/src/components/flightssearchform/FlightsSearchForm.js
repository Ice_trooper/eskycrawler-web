import React from 'react';
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
// import Calendar from 'react-calendar';
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  grid: {
    flexGrow: 1,
  },
}))

export default function FlightsSearchForm(props) {
  const classes = useStyles();

    // state = {
    //     date: new Date(),
    //   }
    
    //   onChange = date => this.setState({ date })
    
    //   render() {
    //     return (
    //       <div>
    //         <Calendar
    //           onChange={this.onChange}
    //           value={this.state.date}
    //         />
    //       </div>
    //     );
    //   }

  return (
    <Grid container className={classes.grid} spacing={2}>
      <Grid item>
        <TextField label="From" variant="outlined"/>
      </Grid>
      <Grid item>
        <TextField label="To" variant="outlined"/>
      </Grid>
      <Grid item>
        <TextField label="Depart" variant="outlined"/>
      </Grid>
      <Grid item>
        <TextField label="Return" variant="outlined"/>
      </Grid>
    </Grid>
  );
}