import React, { useEffect, useState } from 'react'
import FlightsCalendar from 'components/flightsCalendar'
// import FlightsSearchForm from 'components/flightsSearchForm'
import OffersService from 'services/OffersService'
import OfferCardsCarousel from 'components/offerCardsCarousel'
import CalendarService from "services/CalendarService";
import Box from '@material-ui/core/Box'

import { makeStyles } from '@material-ui/styles'
import { darken } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

export default function OffersSection() {
  const useStyles = makeStyles(theme => ({
    section: {
      padding: theme.spacing(3, 0)
    },
    calendarContainer: {
      display: "flex",
      justifyContent: "center",
      margin: theme.spacing(0, 0, 2, 0),

      // '& div.react-calendar': {
      //   // width: 320,
      //   '& div.react-calendar__navigation': {
      //     backgroundColor: theme.palette.primary.main,

      //     '& button': {
      //       color: 'white',
      //       '&:hover': {
      //         backgroundColor: darken(theme.palette.primary.main, 0.1),
      //       }
      //     }
      //   }
      // }
    },
    calendar: {
      font: 'inherit',

      // display: 'flex',
      // justifyContent: 'center',
      // align: 'center',
      // verticalAlign: 'center',
      // marginLeft: 'auto',
      // marginRight: 'auto',
      // backgroundColor: 'green',
      // width: '100%',
      // color: "red",
    },
  }))

  const [range, setRange] = useState([null, null]);
  const [selectedDate, setSelectedDate] = useState(null);
  const [selected, setSelected] = useState(null)
  const [departureDate, setDepartureDate] = useState(null);
  const [returnDate, setReturnDate] = useState(null);
  const [departureDaysCalendar, setDepartureDaysCalendar] = useState([]);
  const [returnDaysCalendar, setReturnDaysCalendar] = useState([]);
  const [offers, setOffers] = useState(null);
  // console.log(offers);

  useEffect(() => {
    if (departureDate !== null || returnDate !== null) {
      OffersService.get(departureDate, returnDate, new Date().toISOString()).then(result => {
        setOffers(result)
      });
    }
  }, [departureDate, returnDate])
  // console.log({ offers })

  const normalizeUTC = (date) => {
    return new Date(date - (date.getTimezoneOffset() * 60000));
  };

  useEffect(() => {
    console.log("EFFECT");
    setDepartureDate(range[0] == null ? null : normalizeUTC(range[0]));
    setReturnDate(range[1] == null ? null : normalizeUTC(range[1]));
    setSelected(selectedDate == null ? null : normalizeUTC(selectedDate))
  }, [range, selectedDate])

  // useEffect(() => {
  //   console.log("DEPARTURES", departureDaysCalendar);
  // }, [departureDaysCalendar])

  // useEffect(() => {
  //   console.log("RETURNS", returnDaysCalendar);
  // }, [returnDaysCalendar])

  useEffect(() => {
    if (departureDate === null && returnDate === null) {
      if (selected === null) {
        //Nothing selected
        console.log("NOTHING SELECTED");
        CalendarService.get(false, null, null, new Date().toISOString()).then((result) => {
          setDepartureDaysCalendar(result);
        });
        CalendarService.get(true, null, null, new Date().toISOString()).then((result) => {
          setReturnDaysCalendar(result);
        });
        setOffers(null);
      }
      else {
        //Selected day
        console.log("SELECTED DAY");
        CalendarService.get(false, null, selected, new Date().toISOString()).then((result) => {
          setDepartureDaysCalendar(result);
        });
        CalendarService.get(true, selected, null, new Date().toISOString()).then((result) => {
          setReturnDaysCalendar(result);
        });
        setOffers(null);
      }
    } else {
      //Selected range
      console.log("SELECTED RANGE");
      CalendarService.get(false, null, returnDate, new Date().toISOString()).then((result) => {
        setDepartureDaysCalendar(result);
      });
      CalendarService.get(true, departureDate, null, new Date().toISOString()).then((result) => {
        setReturnDaysCalendar(result);
      });
    }
  }, [departureDate, returnDate, selected])

  const classes = useStyles();

  return (
    <Box className={classes.section}>
      {/* <div className={classes.dupa}> */}
      <Box className={classes.calendarContainer}>
        <FlightsCalendar
          className={classes.calendar}
          range={range}
          setRange={setRange}
          selectedDate={selectedDate}
          setSelectedDate={setSelectedDate}
          departureDaysCalendar={departureDaysCalendar}
          returnDaysCalendar={returnDaysCalendar}
        />
      </Box>
      {offers !== null && <Typography variant="h4" align="center">Offers: {offers.length}</Typography>}
      <OfferCardsCarousel offers={offers} />
    </Box>
  )
}
