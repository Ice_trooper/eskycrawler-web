import React, { useState, useEffect } from "react";
import Calendar from "react-calendar";
import { makeStyles } from '@material-ui/styles'
import { Typography } from "@material-ui/core";
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff';
import FlightLandIcon from '@material-ui/icons/FlightLand';

export default function FlightsCalendar({ range, setRange, selectedDate, setSelectedDate, departureDaysCalendar, returnDaysCalendar }) {
  const useStyles = makeStyles(theme => ({
    calendar: {
      // display: 'flex',
      // justifyContent: 'center',
      // align: 'center',
      // verticalAlign: 'center',
      // marginLeft: 'auto',
      // marginRight: 'auto',
      // backgroundColor: 'green',
      // width: '100%'
    },
    departuresField: {
      display: 'block',
      backgroundColor: 'green',
      color: 'white',
      fontSize: 11
    },
    takeOffIcon: {
      display: 'inline-flex',
      verticalAlignt: 'middle',
      fontSize: 11,
    },
    returnsField: {
      display: 'block',
      backgroundColor: 'blue',
      color: 'white',
      fontSize: 11
    },
    landIcon: {
      display: 'inline-flex',
      verticalAlignt: 'middle',
      fontSize: 11,
    },
  }))

  const handleChange = (e) => {
    console.log("handleChange");
    setRange(e);
    setSelectedDate(null);
  };

  const handleClickDay = (e) => {
    console.log("handleClickDay");
    if (range[0] != null && range[1] != null) {

      if (e < range[0] || e > range[1]) {
        setSelectedDate(e);
        setRange(e)
      }
      else {
        setRange([null, null]);
      }
    } else {
      if (selectedDate === null) {
        setSelectedDate((e));
        setRange(e);
      }
    }
  };

  const handleClickMonth = () => {
    // console.log("RRRRRR",range);
  }

  const classes = useStyles();

  return (
    <Calendar
      // style={classes.calendar}
      view="month"
      selectRange={true}
      value={range}
      onChange={handleChange}
      onClickDay={handleClickDay}
      onClickMonth={handleClickMonth}
      tileContent={FlightsCalendarTileContent}
    // tileContent={({ activeStartDate, date, view }) =>
    //   view === "month" && date.getDay() === 0 ? <p>It's Sunday!</p> : null
    // }
    // tileContent={(activeStartDate, date, view) => <FlightsCalendarTileContent view={view} date={date}/>}

    // tileContent={({activeStartDate, date, view}) =>
    //   <FlightsCalendarTileContent view={view} date={date}/>
    // }
    />
  );

  function FlightsCalendarTileContent({ activeStartDate, date, view }) {
    const showDepartures = () => {
      if (departureDaysCalendar != null) {
        const d = new Date(date - (date.getTimezoneOffset() * 60000))
        const found = departureDaysCalendar[d.toISOString()];
        if (found != null) {
          return (
            <Typography variant="caption" className={classes.departuresField}>
              <FlightTakeoffIcon className={classes.takeOffIcon}/> {`${found.offersCount}`}
            </Typography>
          )
        }
      }
      return null;
    }

    const showReturns = () => {
      if (returnDaysCalendar != null) {
        const d = new Date(date - (date.getTimezoneOffset() * 60000))
        const found = returnDaysCalendar[d.toISOString()];
        if (found != null) {
          return (
            <Typography variant="caption" className={classes.returnsField}>
              <FlightLandIcon className={classes.landIcon}/> {`${found.offersCount}`}
            </Typography>
          )
        }
      }
      return null;
    }

    return (
      <>
        { view === "month" ? (
          <>
            { showDepartures() }
            { showReturns() }
          </>
        ) : null}
      </>
    )
  }
}

