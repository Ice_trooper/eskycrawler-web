import React, { useState, useContext } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import TextField from '@material-ui/core/TextField'
import Calendar from 'react-calendar';
import { makeStyles } from '@material-ui/styles'
import { Typography, Link } from '@material-ui/core';
import { Context as AirportContext } from 'context/airports/AirportContext'
import { Context as AirlineContext } from 'context/airlines/AirlineContext'
import Box from '@material-ui/core/Box'

const useStyles = makeStyles(theme => ({
  card: {
    borderStyle: 'solid',
    borderWidth: 2,
  },
  content: {
    padding: theme.spacing(1),
  },
  header: {
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: 'orange',
    color: 'black',
    padding: theme.spacing(2),
  },
  headerText: {
    textTransform: 'uppercase',
    letterSpacing: 1,
  },
  subheaderText: {
    textTransform: 'uppercase',
    letterSpacing: 2,
    fontSize: '0.8rem',
  },
  square: {
    height: 50,
    width: 50,
    backgroundColor: "#555",
  },
}))

export default function OfferCard({ offer }) {
  const airports = useContext(AirportContext);
  const airlines = useContext(AirlineContext);
  console.log(airlines)

  const normalizeUTC = (date) => {
    return new Date(date - (date.getTimezoneOffset() * 60000));
  };

  function formatPrice(p) {
    return p.replace(".", ",");
  }

  function formatDate(d) {
    return `${d.getUTCDate() < 10 ? '0' : ''}${d.getUTCDate()}.${d.getUTCMonth() + 1 < 10 ? '0' : ''}${d.getUTCMonth() + 1}.${d.getUTCFullYear()}`
  }

  function formatTime(t) {
    return `${t.getUTCHours() < 10 ? '0' : ''}${t.getUTCHours()}:${t.getUTCMinutes() < 10 ? '0' : ''}${t.getUTCMinutes()}`;
  }

  const fromAirport = airports[offer.outboundFlight.fromAirportId];
  const toAirport = airports[offer.inboundFlight.fromAirportId];
  const departureFromDate = new Date(offer.outboundFlight.fromDate);
  const departureToDate = new Date(offer.outboundFlight.toDate);
  const arrivalFromDate = new Date(offer.inboundFlight.fromDate);
  const arrivalToDate = new Date(offer.inboundFlight.toDate);
  const fromAirline = airlines[offer.outboundFlight.airlineId].name;
  const toAirline = airlines[offer.inboundFlight.airlineId].name;

  const classes = useStyles();

  // <Divider variant="middle">

  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardContent className={classes.content}>
          <Box className={classes.header}>
            <Typography className={classes.subheaderText} align="center">{toAirport.cityName}</Typography>
            <Typography className={classes.headerText} align="center"><strong>{toAirport.countryName}</strong></Typography>
          </Box>
          <Link href={offer.url} display="block" variant="h6" color="inherit" align="center">{formatPrice(offer.price)} PLN</Link>
          <Typography align="center">{fromAirport.name} ({fromAirport.codeName}) {fromAirline}</Typography>
          <Typography align="center">{toAirport.name} ({toAirport.codeName}) {toAirline}</Typography>
          {/* <div className={classes.square} onClick={() => {console.log(offer); console.log(fromDate, fromDate.getTime(), toDate, toDate.getTime())}}/> */}
          <Typography align="center">{formatDate(departureFromDate)} {formatTime(departureFromDate)} - {formatDate(departureToDate)} {formatTime(departureToDate)} {offer.outboundFlight.duration}</Typography>
          <Typography align="center">{formatDate(arrivalFromDate)} {formatTime(arrivalFromDate)} - {formatDate(arrivalToDate)} {formatTime(arrivalToDate)} {offer.inboundFlight.duration}</Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}