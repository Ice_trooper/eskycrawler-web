import React from "react";
// import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/styles";
import OfferCard from "components/offerCard";
import CircularProgress from "@material-ui/core/CircularProgress";
import GridList from "@material-ui/core/GridList";
import { GridListTile } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  gridList: {
    // flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
}));

export default function OfferCardsCarousel({ offers }) {
  const classes = useStyles();

  return (
    <>
      {offers === null || offers === undefined ? (
        // <CircularProgress />
        null
      ) : (
        <GridList className={classes.gridList} cellHeight={260} spacing={8}>
          {offers.map((x, i) => {
            return (
              <GridListTile key={i} cols={2}>
                <OfferCard offer={x}/>
              {/* // <Typography key={i}>
              //   {x.price} {airports[x.outboundFlight.fromAirportId].codeName}
              // </Typography> */}
              </GridListTile>
            );
          })}
        </GridList>
      )}
      {/* <OfferCard offer={offerTemp}/> */}
    </>
  );
}
