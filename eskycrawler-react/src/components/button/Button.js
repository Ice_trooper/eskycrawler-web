import React, { useState } from 'react';
import MuiButton from '@material-ui/core/Button'

export default function Button({
  example,
  ...rest
}) {
    // state = {
    //     date: new Date(),
    //   }
    
    //   onChange = date => this.setState({ date })
    
    //   render() {
    //     return (
    //       <div>
    //         <Calendar
    //           onChange={this.onChange}
    //           value={this.state.date}
    //         />
    //       </div>
    //     );
    //   }
    if(example) console.log('example')

  return (
      <MuiButton {...rest} />
  );
}

Button.defaultProps ={
  color: 'secondary'
}