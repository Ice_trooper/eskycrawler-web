import React from 'react'
import Layout from 'layout'
import { Home } from 'pages'

export default function Root(props) {
  return (
    <Layout>
      <Home />
    </Layout>
  )
}