import React from 'react'
import background from 'assets/images/background-img.jpg'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/styles'
import OffersSection from 'components/offersSection'
// import { Context as AirportContext } from 'context/airports/AirportContext'

const useStyles = makeStyles(theme => ({
  backgroundImg: {
    position: 'relative',
    backgroundImage: `url('${background}')`,
    // backgroundImage: `url('https://cdnstatic1.eskypartners.com/backgrounds/others/test/hero_test_01.jpg')`,
    backgroundPosition: 'top',
    backgroundRepeat: 'no-repeat',
    boxSizing: 'border-box',
    backgroundSize: 'cover',
    height: '85vh',
    width: '100%',
    // height: '70%',
    zIndex: -2
    // minHeight: '400px'
  },
  containerRoot: {
    padding: theme.spacing(0,0),
    // paddingTop: theme.spacing(12),
  },
  paper: {
    // marginTop: theme.spacing(-30),
    margin: theme.spacing(-30, 5, 4, 5),
    padding: theme.spacing(3, 2),
  },
  grid: {
    flexGrow: 1,
  },
}))

export default function Home(props) {
  const classes = useStyles();

  return (
    // <Box className={classes.backgroundImg}>
      <Container classes={{ root: classes.containerRoot }}>
        <Box className={classes.backgroundImg}></Box>
        <Paper className={classes.paper}>
          <Grid container className={classes.grid} justify="center" spacing={2}>
            <Grid item xs={12}>
              <Typography variant='h2' align="center">
                HOME
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <OffersSection align="center" justify="center"/>
            </Grid>
          </Grid>
        </Paper>
      </Container>
    // </Box>
  )
}