import React from 'react'
import MuiDrawer from '@material-ui/core/Drawer';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { makeStyles } from '@material-ui/styles'
import { useTheme } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  drawerPaper: {
    marginTop: theme.spacing(7),

    [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
      marginTop: theme.spacing(6),
    },
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(8),
    },
  },
  drawerModal: {
    zIndex: '1100 !important',
  },
  list: {
    width: 200,
  }
}))

export default function Drawer({
  open,
  setOpen,
}) {
  const classes = useStyles();

  const drawerList = (
    <div
      className={classes.list}
      role="presentation"
    // onClick={toggleDrawer(side, false)}
    // onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <MuiDrawer
      variant='temporary'
      open={open}
      onClose={() => setOpen(false)}
      classes={{
        paper: classes.drawerPaper,
        modal: classes.drawerModal
      }}
      ModalProps={{
        disablePortal: true,
      }}
    >
      {drawerList}
    </MuiDrawer>
  )
}
