import React, { useState } from 'react'
import AppBar from './appBar'
import Drawer from './drawer'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  // appbar: {
  //   background: 'transparent',
  //   boxShadow: 'none',
  //   marginTop: 36,
  //   zIndex: 1201,
  //   // opacity: 0.6,
  // },
  main: {
    // display: 'flex',
    // flexDirection: 'column',
    paddingTop: theme.spacing(5),
    // boxSizing: 'border-box'
    // marginTop: theme.spacing(8),
    // padding: theme.spacing(2),

    // [theme.breakpoints.down('xs')]: {
    //   marginTop: theme.spacing(7),
    // backgroundColor: theme.palette.secondary.main,
    // },
  }
}))

export default function Layout({
  children,
}) {
  const classes = useStyles();

  const [openDrawer, setOpenDrawer] = useState(false)

  return (
    <div>
      <AppBar
        // className={classes.appbar}
        setOpen={setOpenDrawer}
        open={openDrawer}
      />
      <aside>
        <Drawer
          open={openDrawer}
          setOpen={setOpenDrawer}
        />
      </aside>
      <main>
        {children}
      </main>
    </div>
  )
}