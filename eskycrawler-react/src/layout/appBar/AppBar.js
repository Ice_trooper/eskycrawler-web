import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { AppBar as MuiAppBar } from '@material-ui/core'
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

const useStyles = makeStyles(theme => ({
  appBar: ({scrollTrigger}) => ({
    position: 'fixed',
    // backgroundColor: `${theme.palette.primary.main}80`,
    backgroundColor: `${theme.palette.primary.main}${scrollTrigger ? '' : 'A0'}`,
    // background: 'transparent'
    // boxShadow: 'none',
    transition: 'background-color .2s'
  }),
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function AppBar({
  setOpen,
  open
}) {
  const scrollTrigger = useScrollTrigger({disableHysteresis: true, threshold: 20});
  const [auth, setAuth] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const isOpenAccountMenu = Boolean(anchorEl);

  const classes = useStyles({scrollTrigger});

  const handleAccountMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleAccountMenuClose = () => {
    setAnchorEl(null);
  };

  const renderAccountMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id="menu-appbar"
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isOpenAccountMenu}
      onClose={handleAccountMenuClose}
    >
      <MenuItem onClick={handleAccountMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleAccountMenuClose}>My account</MenuItem>
    </Menu>
  );

  return (
    <>
      <MuiAppBar position="static" className={classes.appBar}>
        <Toolbar>
          <IconButton
            className={classes.menuButton}
            edge="start"
            color='inherit'
            aria-label="Menu-app"
            aria-controls="menu-appbar"
            onClick={() => setOpen(!open)}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            CheapFlights
          </Typography>
          <IconButton edge="end" color="inherit">
            <SearchIcon />
          </IconButton>
          {auth && (
            <div>
              <IconButton
                aria-label="account of current user"
                aria-controls="account-appbar"
                aria-haspopup="true"
                onClick={handleAccountMenuOpen}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              {/* <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={isOpenAccountMenu}
                onClose={handleAccountMenuClose}
              >
                <MenuItem onClick={handleAccountMenuClose}>Profile</MenuItem>
                <MenuItem onClick={handleAccountMenuClose}>My account</MenuItem>
              </Menu> */}
            </div>
          )}
        </Toolbar>
      </MuiAppBar>
      {renderAccountMenu}
    </>
  )
}