import { RestGET } from "utils/Rest"
import Routes from "constants/Routes"

const URL = Routes.AIRLINES;

export const get = async () => {
  try {
    let airlines = await RestGET(URL)
    airlines = airlines.reduce( (acc ,item) => {
      return {
        ...acc,
        [item.id]: item,
      }
    }, {})
    return airlines;
  } catch (error) {
  }
}

export default {
  get,
}