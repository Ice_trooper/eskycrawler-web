import { RestGET } from "utils/Rest"
import Routes from "constants/Routes"

const URL = Routes.AIRPORTS;

export const get = async () => {
  try {
    let airports = await RestGET(URL)
    airports = airports.reduce( (acc ,item) => {
      return {
        ...acc,
        [item.id]: item,
      }
    }, {})
    return airports;
  } catch (error) {
  }
}

export default {
  get,
}