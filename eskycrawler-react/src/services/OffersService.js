import Rest, { RestGET } from "utils/Rest"
import Routes from "constants/Routes"

const URL = Routes.OFFERS;

const get = async (departureDate, returnDate, start) => {

  try {
    const response = await RestGET(URL, {
      'departureDate': departureDate,
      'returnDate': returnDate,
      'start': start,
    })
    // const response = Rest({
    //   method: 'GET',
    //   url: URL,
    //   params: {}
    // })
    return response;
  } catch (error) {
  }
}

export default {
  get,
}
