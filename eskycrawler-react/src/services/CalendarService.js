import Rest, { RestGET } from "utils/Rest"
import Routes from "constants/Routes"

const URL = Routes.CALENDAR;

const get = async (returns, departureDate, returnDate, start) => {

  try {
    let response = await RestGET(URL, {
      'returns': returns,
      'departureDate': departureDate,
      'returnDate': returnDate,
      'start': start
    })
    response = response.reduce( (acc ,item) => {
      return {
        ...acc,
        [new Date(item.date).toISOString()]: item,
      }
    }, {})
    // const response = Rest({
    //   method: 'GET',
    //   url: URL,
    //   params: {}
    // })
    return response;
  } catch (error) {
  }
}

export default {
  get,
}
