
import config from 'config.json'

const base = config.apiUrl;

export default {
  OFFERS: `${base}/offers`,
  AIRPORTS: `${base}/airports`,
  AIRLINES: `${base}/airlines`,
  CALENDAR: `${base}/calendar`,
}