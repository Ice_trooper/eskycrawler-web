import Axios from 'axios'

export const Rest = async (params) => {
  console.log({params})
  try {
    const response = await Axios({
      headers: {
        'Content-Type': 'apllication/json'
      },
      ...params
    });
    return response.data;
  } catch (error) {
    console.error({error});
  }
}

export const RestGET = async (url, params) => {
  return await Rest({
    method: 'GET',
    url,
    params,
  })
}

export default Rest;