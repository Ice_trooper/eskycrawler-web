import React, { useState } from 'react'
import { createContext } from "react";
import { useEffect } from 'react';
import AirlineService from 'services/AirlineService';

export const Context = createContext();

export default function AirlineProvider({
  children,
}) {
  const [airlines, setAirlines] = useState({})
  useEffect(() => {
    AirlineService.get().then(res => setAirlines(res))
  }, [])

  return (
    <Context.Provider value={airlines}>
      {children}
    </Context.Provider>
  )
}
