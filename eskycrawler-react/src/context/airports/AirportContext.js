import React, { useState } from 'react'
import { createContext } from "react";
import { useEffect } from 'react';
import AirportService from 'services/AirportService';

export const Context = createContext();

export default function AirportProvider({
  children,
}) {
  const [airports, setAirports] = useState({})
  useEffect(() => {
    AirportService.get().then(res => setAirports(res))
  }, [])

  return (
    <Context.Provider value={airports}>
      {children}
    </Context.Provider>
  )
}
